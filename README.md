# Find Joe

My approach to Find Joe exercise with using XGBoost - an implementation of gradient boosted decision trees designed for speed and performance.

## Results

Precision-Recall AUC score: 0.902
Precision-Recall curve (as well as ROC curve) are in /imgs folder, properly named, and in notebook (does not show inserted images in web).

## Notes

I've always hated developing stuff in Jupyter notebooks so I haven't done it in a while. I've decided to give it another try but no, I still prefer much more to code everyting in .py scripts.